### 7 第三方软件安装 ###

  本章所述的软件是可以选择性安装的，没有这些软件OpenFOAM也一样可以完整良好地运行。但是在一些特定任务中，这些软件可能非常有用。

#### 7.1 安装 *pyFoam* ####

  参考<http://openfoamwiki.net/index.php/Contrib_PyFoam#Istallation>网站中关于*pyFoam*安装的相关信息。

#### 7.2 安装 *swak4foam* ####

  参考<http://openfoamwiki.net/index.php/Contrib/swak4foam>网站中关于*swak4foam*安装的相关信息。

#### 7.3 编译外部函数库 ####

  用外部函数库来扩展OpenFOAM的功能是可以做到的，也就是说这些OpenFOAM函数库来自其他开源代码，而不是OpenFOAM的开发者们。一个能够很好说明这样一种外部函数库的例子，是来自网站<https://github.com/AlbertoPa/dynamicSmagorinsky>的大涡湍流模型，其源代码储存在```OpenFOAM/AlbertoPa```路径下。
  外部函数库需要通过```wmake libso```工具来进行编译，当OpenFOAM函数库被修改时也同样如此。通过键盘键入```wmake libso```的理由是充分的，因为*wmake*所需要的所有信息都存储在```Make/files```和```Make/options```两个文件中。这些文件告诉*wmake*和编译器到哪里找到必要的函数库，在哪里生成最终的可执行程序。关于这两个文件的更多细节描述可以参见58.2.2节。
  如果使用外部函数库的话，这些信息同样需要传递给求解器。这部分内容可以参见11.3.3节。

```
cd OpenFOAM/AlbertoPa/dynamicSmagorinsky
wmake libso
```
                                      表19：函数库编译
